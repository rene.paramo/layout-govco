//  * Gov.co (https://www.gov.co) - Gobierno de Colombia
//  *  - Componente: script
//  *  - Version: 4.0.0

// ENTRADAS DE TEXTO Y BARRA DE BUSQUEDA
function asignaFuncion(e, f, elements) {
  for(let i of elements) {
    i.addEventListener(e, f, false);
  }
}

// BARRA DE BUSQUEDA
var inputSearch = document.querySelectorAll(".search-input");
asignaFuncion("keyup", activeInput, inputSearch);
asignaFuncion("keydown", keydownInputSearch, inputSearch);

var buttonClean = document.querySelectorAll(".clean");
asignaFuncion("click", cleanInput, buttonClean);

function activeInput(element) {
  var parent = this.parentNode;
  var existsClass = parent.classList.contains('active');
  if(this.value === '') {
    parent.classList.remove('active');
  } else if(!existsClass) {
    parent.classList.add('active');
    asignarFuncionaFlechasItems(parent, this);
  }
}

function asignarFuncionaFlechasItems(parentInput, input) {
  var parentItems = parentInput.nextElementSibling;
  var items = parentItems.querySelectorAll("ul li a");

  for (let item of items) {
    item.addEventListener("keydown", function(event) {
      keysUpDown(event, parentItems, input);
    });
  }
}

function keydownInputSearch(element) {
  var parentInput = this.parentNode;
  var parentItems = parentInput.nextElementSibling;
  var parentUl = parentItems.querySelector('.options-govco');

  parentUl.onscroll = function() {
    var visibleItems = this.querySelectorAll("li a");
    if (document.activeElement == visibleItems[0]) {
      this.scrollTop = 0;
    }
  };

  keysUpDown(element, parentItems, this);
}

function keysUpDown(e, container, input) {
  console.log(e);
  // Key up
  if (e.which == 38) {
    up(container, input);
  }

  // Key down
  if (e.which == 40) {
    down(container, input);
  }
}

function down(container, input) {
  var active = document.activeElement;
  var items = container.querySelectorAll("li a");
  if (active === input) {
    items[0].focus();
  }
  else {
    for (var i = 0; i < items.length - 1; i++) {
      if (active === items[i]) {
        items[i + 1].focus();
      }
    }
  }
}

function up(container, input) {
  var active = document.activeElement;
  var itemsList = container.querySelectorAll("li:not(.none) a");
  if (active === itemsList[0]) {
    input.focus();
  }
  else {
    for (var i = 1; i < itemsList.length; i++) {
      if (active === itemsList[i]){
        itemsList[i - 1].focus();
      }
    }
  }
}

function cleanInput(element) {
  var input = this.previousElementSibling;
  var parent = this.parentNode;
  input.value = '';
  parent.classList.remove('active');
}

// VOLVER ARRIBA
var volverArriba = document.querySelectorAll(".volver-arriba-govco");
volverArriba.forEach(e => {
  e.addEventListener("click", backGoToUp, false);
});

function backGoToUp() {
  document.body.scrollTop = document.documentElement.scrollTop = 0;
}

// ENTRADAS DE TEXTO
var inputDisabled = document.querySelectorAll('input[disabled]');

for(let d of inputDisabled) {
  var containerDisabled = d.closest('.container-texto-govco');
  containerDisabled.classList.add('disabled-govco');
}
// contador
var inputContador = document.querySelectorAll('input[typeData="accountant"]');
asignaFuncion("keyup", activeInputContador, inputContador);

function activeInputContador(element) {
  var parentInputContador = this.parentNode;
  var span = parentInputContador.querySelector(".numero-contador-texto-govco");
  span.innerHTML = this.value.length;
}

// password
var iconInputPassword = document.querySelectorAll('.icon-texto-govco');
asignaFuncion("click", activeIconInputPassword, iconInputPassword);

function activeIconInputPassword(element) {
  var parentPassword = this.parentNode;
  var inputPassword = parentPassword.querySelector('.campo-texto-govco');
  var visiblePassword = parentPassword.querySelector('.eye-texto-govco');
  var hidePassword = parentPassword.querySelector('.eye-slash-texto-govco');

  if(inputPassword.getAttribute('disabled') === null) {
    if (inputPassword.type == 'password') {
      inputPassword.type = 'text';
      visiblePassword.classList.remove('none');
      hidePassword.classList.add('none');
    } else {
      inputPassword.type = 'password';
      hidePassword.classList.remove('none');
      visiblePassword.classList.add('none');
    }
  }
}

var inputContrasenia = document.querySelectorAll('input[typeData="password"]');
asignaFuncion("keyup", activeInputContrasenia, inputContrasenia);

function activeInputContrasenia(element) {
  var expresionRegularP = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
  var textExito = "Contraseña correcta";
  var textError = "Contraseña incorrecta, debe contener mínimo ocho (8) caracteres, un número, una letra minúscula, una letra mayúscula, un carácter especial.";

  if (expresionRegularP.test(this.value) && this.classList.contains("success") === false) {
    this.classList.remove('error');
    this.classList.add('success');
    crearMensaje(this, textExito, 'success');
  } else if(expresionRegularP.test(this.value) === false && this.classList.contains("error") === false) {
    this.classList.remove('success');
    this.classList.add('error');
    crearMensaje(this, textError, 'error');
  }
}

// correo electronico
var inputCorreo = document.querySelectorAll('input[typeData="mail"]');
asignaFuncion("keyup", activeInputCorreo, inputCorreo);

function activeInputCorreo(element) {
  var expresionRegularE = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
  var textExito = "Correo electrónico válido";
  var textError = "Correo electrónico no válido";

  if (expresionRegularE.test(this.value) && this.classList.contains("success") === false) {
    this.classList.remove('error');
    this.classList.add('success');
    crearMensaje(this, textExito, 'success');
  } else if(expresionRegularE.test(this.value) === false && this.classList.contains("error") === false) {
    this.classList.remove('success');
    this.classList.add('error');
    crearMensaje(this, textError, 'error');
  }
}

// teléfono
var inputTelefono = document.querySelectorAll('input[typeData="phone"]');
asignaFuncion("keyup", activeInputTelefono, inputTelefono);

function activeInputTelefono(element) {
  var expresionRegularE = /^[+]\(?(\d{2})\)? [-]?(\d{3})[-]? (\d{7,10})$/;
  var textExito = "Número de teléfono válido";
  var textError = "Número de teléfono no válido";

  if (expresionRegularE.test(this.value) && this.classList.contains("success") === false) {
    this.classList.remove('error');
    this.classList.add('success');
    crearMensaje(this, textExito, 'success');
  } else if(expresionRegularE.test(this.value) === false && this.classList.contains("error") === false) {
    this.classList.remove('success');
    this.classList.add('error');
    crearMensaje(this, textError, 'error');
  }
}

function crearMensaje(e, text, type) {
  var dataMensajes = {
    'success': {
      'id': 'campoSuccess-id',
      'aria-invalid': 'false',
      'class': 'success-texto-govco',
      'role': 'role',
      'aria-live': 'polite',
    },
    'error': {
      'id': 'campoWarning-id',
      'aria-invalid': 'true',
      'class': 'error-texto-govco',
      'role': 'alert',
      'aria-live': 'assertive',
    }
  };

  var parentInput = e.closest('.container-texto-govco');
  var spanOld = parentInput.querySelector('.alert-texto-govco');
  if(spanOld) { parentInput.removeChild(spanOld); }
  var newSpan = document.createElement('span');
  var span = parentInput.appendChild(newSpan);

  e.setAttribute('aria-describedby', dataMensajes[type]['id']);
  e.setAttribute('aria-invalid', dataMensajes[type]['aria-invalid']);

  span.textContent = text;
  span.classList.add(dataMensajes[type]['class'], 'alert-texto-govco');
  span.id = dataMensajes[type]['id'];
  span.setAttribute('role', dataMensajes[type]['role']);
  span.setAttribute('aria-live', dataMensajes[type]['aria-live']);
}

// MENU DE NAVEGACION
document.addEventListener("DOMContentLoaded", function(){
  /////// Prevent closing from click inside dropdown
  document.querySelectorAll('.dropdown-menu').forEach(function(element){
    element.addEventListener('click', function (e) {
      e.stopPropagation();
    });
  });

  document.querySelectorAll('.navbar-menu-govco a.dir-menu-govco').forEach(function(element){
    element.addEventListener("click", eventClickItem, false);
  });
}); 

function eventClickItem() {
  const parentNavbar = this.closest('.navbar-menu-govco');
  parentNavbar.querySelectorAll('a.active').forEach(function(element){
      element.classList.remove('active');
  });

  this.classList.add('active');
  const container = this.closest('.nav-item');
  const itemParent =  container.querySelector('.nav-link');
  itemParent.classList.add('active');
}

/**
 * Gov.co (https://www.gov.co) - Gobierno de Colombia
 *  - Componente: Buscador
 *  - Version: 4.0.0
 */

 function asignaFuncion(e, f, elements) {
  for(let i of elements) {
    i.addEventListener(e, f, false);
  }
}

var inputSearch = document.querySelectorAll(".search-input");
asignaFuncion("keyup", activeInput, inputSearch);
asignaFuncion("keydown", keydownInputSearch, inputSearch);

var buttonClean = document.querySelectorAll(".clean");
asignaFuncion("click", cleanInput, buttonClean);

function activeInput(element) {
  var parent = this.parentNode;
  var existsClass = parent.classList.contains('active');
  if(this.value === '') {
    parent.classList.remove('active');
  } else if(!existsClass) {
    parent.classList.add('active');
    asignarFuncionaFlechasItems(parent, this);
  }
}

function asignarFuncionaFlechasItems(parentInput, input) {
  var parentItems = parentInput.nextElementSibling;
  var items = parentItems.querySelectorAll("ul li a");

  for (let item of items) {
    item.addEventListener("keydown", function(event) {
      keysUpDown(event, parentItems, input);
    });
  }
}

function keydownInputSearch(element) {
  var parentInput = this.parentNode;
  var parentItems = parentInput.nextElementSibling;
  var parentUl = parentItems.querySelector('.options-govco');

  parentUl.onscroll = function() {
    var visibleItems = this.querySelectorAll("li a");
    if (document.activeElement == visibleItems[0]) {
      this.scrollTop = 0;
    }
  };

  keysUpDown(element, parentItems, this);
}

function keysUpDown(e, container, input) {
  console.log(e);
  // Key up
  if (e.which == 38) {
    up(container, input);
  }

  // Key down
  if (e.which == 40) {
    down(container, input);
  }
}

function down(container, input) {
  var active = document.activeElement;
  var items = container.querySelectorAll("li a");
  if (active === input) {
    items[0].focus();
  }
  else {
    for (var i = 0; i < items.length - 1; i++) {
      if (active === items[i]) {
        items[i + 1].focus();
      }
    }
  }
}

function up(container, input) {
  var active = document.activeElement;
  var itemsList = container.querySelectorAll("li:not(.none) a");
  if (active === itemsList[0]) {
    input.focus();
  }
  else {
    for (var i = 1; i < itemsList.length; i++) {
      if (active === itemsList[i]){
        itemsList[i - 1].focus();
      }
    }
  }
}

function cleanInput(element) {
  var input = this.previousElementSibling;
  var parent = this.parentNode;
  input.value = '';
  parent.classList.remove('active');
}

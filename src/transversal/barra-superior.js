/**
 * Gov.co (https://www.gov.co) - Gobierno de Colombia
 *  - Componente: Barra superior
 *  - Version: 4.0.0
 */

 var translateElement = document.querySelector(".icon-barra-superior-idioma");
 translateElement.addEventListener("click", translate, false);

 function translate() {
  // ... // Implementar traducción
 }
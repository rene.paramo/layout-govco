/**
 * Gov.co (https://www.gov.co) - Gobierno de Colombia
 *  - Componente: Desplegables
 *  - Version: 4.0.0
 */

window.onload = function () {
    crearLista("lista-desplegables");
    crearListaFiltroBusqueda("lista-filtro-busqueda");
    crearListaCasillasVerificacion("lista-casillas-verificacion");
    document.addEventListener("click", closeAllSelect);

    var miCalendario = document.getElementById('miCalendario');
    var inputNode = miCalendario.querySelector('input');
    var dialogNode = miCalendario.querySelector('[role=dialog]');

    var datePicker = new DatePicker(inputNode, dialogNode);
    datePicker.init();
}

// Funciones de comportamiento de desplegables

function toggleSelect(elmnt) {
    if (elmnt) {
        elmnt.nextSibling.classList.toggle("desplegable-hide");
        elmnt.classList.toggle("desplegable-arrow-active");
    }
}

function closeAllSelect() {
    selectsSelecteds = document.querySelectorAll(".desplegable-govco .desplegable-selected-option:not(.dont-close)");
    if (selectsSelecteds) {
        selectsSelecteds.forEach(function (e) {
            if (e.classList.contains("desplegable-arrow-active")) {

                e.classList.remove("desplegable-arrow-active");
                e.nextSibling.classList.add("desplegable-hide");
            }
        })
    }
}

function openSelect(elmnt) {
    if (elmnt) {
        elmnt.nextSibling.classList.remove("desplegable-hide");
        elmnt.classList.add("desplegable-arrow-active");
    }
}

function clearInput(e) {
    input = e.closest(".desplegable-selected-option").querySelector("input");
    input.value = "";
    filterFunction(e);
    clearIcon = e.closest(".desplegable-selected-option").querySelector("span.btn-clear-desplegable-govco");
    clearIcon.classList.add("d-none");
}

function filterFunction(e) {
    var input, filter, div, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    myDropdown = document.getElementById("myDropdown");
    div = myDropdown.getElementsByTagName("div");
    for (i = 0; i < div.length; i++) {
        txtValue = div[i].textContent || div[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            div[i].style.display = "";
        } else {
            div[i].style.display = "none";
        }
    }
    if (input.value != "" && input.value != undefined && input.value != null) {

        clearIcon = e.closest(".desplegable-selected-option").querySelector("span.btn-clear-desplegable-govco");
        clearIcon.classList.remove("d-none");
    }
}

function crearLista(idSelectElement) {

    var selectSelected, selectItems, options;
    select = document.getElementById(idSelectElement);
    if (select == undefined || select == null) {
        return false;
    }
    selectTag = select.querySelector("select");
    // Se crea estructura de listado seleccionable

    selectSelected = document.createElement("DIV");
    selectSelected.setAttribute("class", "desplegable-selected-option");
    selectSelected.innerHTML = selectTag.options[selectTag.selectedIndex].innerHTML;
    selectSelected.addEventListener("click", function (e) {
        e.stopPropagation();
        toggleSelect(this);
    });
    select.appendChild(selectSelected);
    selectItems = document.createElement("DIV");
    selectItems.setAttribute("class", "desplegable-items desplegable-hide");

    for (j = 1; j < selectTag.length; j++) {
        // Se construyen opciones de selección
        options = document.createElement("DIV");
        options.innerHTML = selectTag.options[j].innerHTML;
        options.addEventListener("click", function (e) {

            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    yl = y.length;
                    for (k = 0; k < yl; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    closeAllSelect()
                    break;
                }
            }
            h.click();
        });
        selectItems.appendChild(options);
    }
    select.appendChild(selectItems);
}


function crearListaFiltroBusqueda(idSelectElement) {
    var selectSelected, selectItems, options;
    select = document.getElementById(idSelectElement);

    if (select == undefined || select == null) {
        return false;
    }

    selectTag = select.querySelector("select");

    // Se construye estructura de desplegable con filtro de búsqueda

    selectSelected = document.createElement("DIV");
    selectSelected.setAttribute("class", "desplegable-selected-option search-filter");
    selectSelected.addEventListener("click", function (e) {
        e.stopPropagation();
        openSelect(this);
        filterFunction(this);
    });

    select.appendChild(selectSelected);

    selectItems = document.createElement("DIV");
    selectItems.setAttribute("class", "desplegable-items desplegable-hide");
    selectItems.setAttribute("id", "myDropdown");
    input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "Ingrese texto...");
    input.setAttribute("id", "myInput");
    input.setAttribute("onkeyup", "filterFunction(this)");
    input.setAttribute("autocomplete", "off");
    input.setAttribute("class", "browser-default");
    selectSelected.appendChild(input);
    iconClear = document.createElement("span");
    iconClear.setAttribute("onclick", "clearInput(this)");
    iconClear.setAttribute("class", "btn-clear-desplegable-govco d-none");
    selectSelected.appendChild(iconClear);


    for (j = 1; j < selectTag.length; j++) {
        // Se construyen opciones de selección
        options = document.createElement("DIV");
        options.innerHTML = selectTag.options[j].innerHTML;
        options.addEventListener("click", function (e) {

            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;

                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    yl = y.length;
                    for (k = 0; k < yl; k++) {
                        y[k].removeAttribute("class");
                    }
                    input = this.closest(".desplegable-govco").querySelector(".desplegable-selected-option input");
                    input.value = this.innerHTML;
                    closeAllSelect()
                    break;
                }
            }
            h.click();
        });
        selectItems.appendChild(options);
    }
    select.appendChild(selectItems);
}

function crearListaCasillasVerificacion(idSelectElement) {

    var selectSelected, selectItems, options;
    select = document.getElementById(idSelectElement);

    if (select == undefined || select == null) {
        return false;
    }

    selectTag = select.querySelector("select");

    // Se crea estructura de selección e items de selección

    selectSelected = document.createElement("DIV");
    selectSelected.setAttribute("class", "desplegable-selected-option");
    selectSelected.innerHTML = selectTag.options[selectTag.selectedIndex].innerHTML;
    selectSelected.addEventListener("click", function (e) {
        e.stopPropagation();
        openSelect(this);
    });
    select.appendChild(selectSelected);
    // Se crea estructura contenedora de las opciones
    selectItems = document.createElement("DIV");
    selectItems.setAttribute("class", "desplegable-items desplegable-hide");

    for (j = 1; j < selectTag.length; j++) {
        // Se construyen las opciones
        options = document.createElement("DIV");

        options.addEventListener("click", function (e) {
            var values = [];
            var selectSelected = this.closest(".desplegable-govco").querySelector(".desplegable-selected-option");
            this.closest(".desplegable-items").querySelectorAll("input[type=checkbox]").forEach(
                function (e) {
                    if (e.checked)
                        values.push(e.getAttribute("text"));
                }
            )
            selectSelected.innerHTML = values.length > 0 ? values.join(', ') : "Escoger";
        });
        selectItems.appendChild(options);
        label = document.createElement("label");
        label.setAttribute("class", "checkbox-govco");
        options.appendChild(label);
        input = document.createElement("input");
        input.setAttribute("id", selectTag.options[j].value);
        input.setAttribute("type", "checkbox");
        input.setAttribute("text", selectTag.options[j].innerHTML);
        input.setAttribute("class", "browser-default");
        label.appendChild(input);
        labelText = document.createElement("label");
        labelText.setAttribute("for", selectTag.options[j].value);
        labelText.innerHTML = selectTag.options[j].innerHTML;
        label.appendChild(labelText);
    }
    select.appendChild(selectItems);
}


//calendario
// Calendario
// Calendario
var DatePickerDay = function (domNode, datepicker, index, row, column) {

    this.index = index;
    this.row = row;
    this.column = column;

    this.day = new Date();

    this.domNode = domNode;
    this.datepicker = datepicker;

    this.keyCode = Object.freeze({
        'TAB': 9,
        'ENTER': 13,
        'ESC': 27,
        'SPACE': 32,
        'PAGEUP': 33,
        'PAGEDOWN': 34,
        'END': 35,
        'HOME': 36,
        'LEFT': 37,
        'UP': 38,
        'RIGHT': 39,
        'DOWN': 40
    });
};

DatePickerDay.prototype.init = function () {
    this.domNode.setAttribute('tabindex', '-1');
    this.domNode.addEventListener('mousedown', this.handleMouseDown.bind(this));
    this.domNode.addEventListener('keydown', this.handleKeyDown.bind(this));

    this.domNode.innerHTML = '-1';

};

DatePickerDay.prototype.isDisabled = function () {
    return this.domNode.classList.contains('disabled');
};

DatePickerDay.prototype.updateDay = function (disable, day) {

    if (disable) {
        this.domNode.classList.add('disabled');
    }
    else {
        this.domNode.classList.remove('disabled');
    }

    this.day = new Date(day);

    this.domNode.innerHTML = this.day.getDate();
    this.domNode.setAttribute('tabindex', '-1');
    this.domNode.removeAttribute('aria-selected');

    var d = this.day.getDate().toString();
    if (this.day.getDate() < 9) {
        d = '0' + d;
    }

    var m = this.day.getMonth() + 1;
    if (this.day.getMonth() < 9) {
        m = '0' + m;
    }

    this.domNode.setAttribute('data-date', this.day.getFullYear() + '-' + m + '-' + d);

};

DatePickerDay.prototype.handleKeyDown = function (event) {
    var flag = false;

    switch (event.keyCode) {

        case this.keyCode.ESC:
            this.datepicker.hide();
            break;

        case this.keyCode.TAB:
            this.datepicker.cancelButtonNode.focus();
            flag = true;
            break;

        case this.keyCode.ENTER:
        case this.keyCode.SPACE:
            this.datepicker.setTextboxDate(this.day);
            this.datepicker.hide();
            flag = true;
            break;

        case this.keyCode.RIGHT:
            this.datepicker.moveFocusToNextDay();
            flag = true;
            break;

        case this.keyCode.LEFT:
            this.datepicker.moveFocusToPreviousDay();
            flag = true;
            break;

        case this.keyCode.DOWN:
            this.datepicker.moveFocusToNextWeek();
            flag = true;
            break;

        case this.keyCode.UP:
            this.datepicker.moveFocusToPreviousWeek();
            flag = true;
            break;

        case this.keyCode.PAGEUP:
            if (event.shiftKey) {
                this.datepicker.moveToPreviousYear();
            }
            else {
                this.datepicker.moveToPreviousMonth();
            }
            flag = true;
            break;

        case this.keyCode.PAGEDOWN:
            if (event.shiftKey) {
                this.datepicker.moveToNextYear();
            }
            else {
                this.datepicker.moveToNextMonth();
            }
            flag = true;
            break;

        case this.keyCode.HOME:
            this.datepicker.moveFocusToFirstDayOfWeek();
            flag = true;
            break;

        case this.keyCode.END:
            this.datepicker.moveFocusToLastDayOfWeek();
            flag = true;
            break;
    }

    if (flag) {
        event.stopPropagation();
        event.preventDefault();
    }

};

DatePickerDay.prototype.handleMouseDown = function (event) {
    if (this.isDisabled()) {
        this.datepicker.moveFocusToDay(this.date);
    }
    else {
        this.datepicker.setTextboxDate(this.day);
        this.datepicker.hide();
    }

    event.stopPropagation();
    event.preventDefault();

};

var CalendarButtonInput = CalendarButtonInput || {};
var DatePickerDay = DatePickerDay || {};

var DatePicker = function (inputNode, dialogNode) {
    this.dayLabels = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    this.monthLabels = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

    this.inputNode = inputNode;
    this.dialogNode = dialogNode;

    this.dateInput = new CalendarButtonInput(this.inputNode, this);

    this.MonthYearNode = this.dialogNode.querySelector('.month-year');
    this.MonthsYearNode = this.dialogNode.querySelector('button.show-months-year');
    console.log(this.MonthsYearNode)

    this.prevYearNode = this.dialogNode.querySelector('.prev-year');
    this.prevMonthNode = this.dialogNode.querySelector('.prev-month');
    this.prevDecadeNode = this.dialogNode.querySelector('.prev-decade');
    this.nextMonthNode = this.dialogNode.querySelector('.next-month');
    this.nextYearNode = this.dialogNode.querySelector('.next-year');
    this.nextDecadeNode = this.dialogNode.querySelector('.next-decade');

    this.okButtonNode = this.dialogNode.querySelector('button[value="ok"]');
    this.cancelButtonNode = this.dialogNode.querySelector('button[value="cancel"]');

    this.tbodyNode = this.dialogNode.querySelector('table.dates tbody');

    this.dateTableNode = this.dialogNode.querySelector('table.dates');

    this.monthsTableNode = this.dialogNode.querySelector('table.months');
    this.buttonsCellsMonths = this.monthsTableNode.querySelectorAll('button');

    this.yearsTableNode = this.dialogNode.querySelector('table.years');
    this.buttonsCellsYears = this.yearsTableNode.querySelectorAll('button');

    this.lastRowNode = null;

    this.days = [];

    this.focusDay = new Date();
    this.selectedDay = new Date(0, 0, 1);

    this.isMouseDownOnBackground = false;

    this.keyCode = Object.freeze({
        'TAB': 9,
        'ENTER': 13,
        'ESC': 27,
        'SPACE': 32,
        'PAGEUP': 33,
        'PAGEDOWN': 34,
        'END': 35,
        'HOME': 36,
        'LEFT': 37,
        'UP': 38,
        'RIGHT': 39,
        'DOWN': 40
    });

};

DatePicker.prototype.init = function () {
    var that = this;
    this.dateInput.init();
    this.MonthsYearNode.addEventListener('click', this.handleMonthsYear.bind(this));

    this.prevMonthNode.addEventListener('click', this.handlePreviousMonthButton.bind(this));
    this.nextMonthNode.addEventListener('click', this.handleNextMonthButton.bind(this));

    this.prevYearNode.addEventListener('click', this.handlePreviousYearButton.bind(this));
    this.nextYearNode.addEventListener('click', this.handleNextYearButton.bind(this));

    this.prevMonthNode.addEventListener('keydown', this.handlePreviousMonthButton.bind(this));
    this.nextMonthNode.addEventListener('keydown', this.handleNextMonthButton.bind(this));

    this.prevYearNode.addEventListener('keydown', this.handlePreviousYearButton.bind(this));
    this.nextYearNode.addEventListener('keydown', this.handleNextYearButton.bind(this));

    this.buttonsCellsMonths.forEach(function (button) {
        button.addEventListener('click', that.handleMonthButton.bind(that));
    });
    this.buttonsCellsYears.forEach(function (button) {
        button.addEventListener('click', that.handleYearButton.bind(that));
    })

    document.body.addEventListener('mousedown', this.handleBackgroundMouseDown.bind(this), true);
    document.body.addEventListener('mouseup', this.handleBackgroundMouseUp.bind(this), true);

    // Create Grid of Dates

    this.tbodyNode.innerHTML = '';
    var index = 0;
    for (var i = 0; i < 6; i++) {
        var row = this.tbodyNode.insertRow(i);
        this.lastRowNode = row;
        row.classList.add('dateRow');
        for (var j = 0; j < 7; j++) {
            var cell = document.createElement('td');
            cell.classList.add('date-cell');
            var cellButton = document.createElement('button');
            cellButton.classList.add('date-button');
            cell.appendChild(cellButton);
            row.appendChild(cell);
            var dpDay = new DatePickerDay(cellButton, this, index, i, j);
            dpDay.init();
            this.days.push(dpDay);
            index++;
        }
    }

    this.monthsTableNode.classList.add('d-none');
    this.yearsTableNode.classList.add('d-none');

    this.updateGrid();
    this.setFocusDay(false);
};

DatePicker.prototype.updateGrid = function () {

    var i, flag;
    var fd = this.focusDay;

    this.MonthYearNode.innerHTML = this.monthLabels[fd.getMonth()] + ' ' + fd.getFullYear();

    var firstDayOfMonth = new Date(fd.getFullYear(), fd.getMonth(), 1);
    var daysInMonth = new Date(fd.getFullYear(), fd.getMonth() + 1, 0).getDate();
    var dayOfWeek = firstDayOfMonth.getDay();

    firstDayOfMonth.setDate(firstDayOfMonth.getDate() - dayOfWeek);

    var d = new Date(firstDayOfMonth);

    for (i = 0; i < this.days.length; i++) {
        flag = d.getMonth() != fd.getMonth();
        this.days[i].updateDay(flag, d);
        if ((d.getFullYear() == this.selectedDay.getFullYear()) &&
            (d.getMonth() == this.selectedDay.getMonth()) &&
            (d.getDate() == this.selectedDay.getDate())) {
            this.days[i].domNode.setAttribute('aria-selected', 'true');
        }
        d.setDate(d.getDate() + 1);
    }

    if ((dayOfWeek + daysInMonth) < 36) {
        this.hideLastRow();
    }
    else {
        this.showLastRow();
    }

};

DatePicker.prototype.updateYearGrid = function () {
    var fd = this.focusDay;
    var year = fd.getFullYear();

    this.buttonsCellsYears.forEach(function (element, index) {

        element.innerHTML = year + index - 6;
        element.setAttribute('tabindex', '-1');
        element.setAttribute('data-date', year + index - 6);
    })
    var buttonYear = this.yearsTableNode.querySelector("button[data-date='" + year + "']");
    buttonYear.setAttribute('tabindex', '0');
};

DatePicker.prototype.updateGridMonth = function () {

    var fd = this.focusDay;
    this.MonthYearNode.innerHTML = fd.getFullYear();
};

DatePicker.prototype.updateGridYear = function () {
    var fd = this.focusDay;
    this.MonthYearNode.innerHTML = (fd.getFullYear() - 6) + " - " + (fd.getFullYear() + 5);
};

DatePicker.prototype.handleMonthsYear = function (event) {
    if (!this.dateTableNode.classList.contains('d-none')) {
        this.showMonthsYear();
        this.updateGridMonth();
    }
    else if (!this.monthsTableNode.classList.contains('d-none')) {
        this.showYearsDecade();
        this.updateGridYear();
    }
    else if (!this.yearsTableNode.classList.contains('d-none')) {
        this.showDaysMonth();
        this.updateGrid();
    }
}

DatePicker.prototype.showMonthsYear = function () {
    this.dateTableNode.classList.add('d-none');
    this.prevMonthNode.classList.add('d-none');
    this.nextMonthNode.classList.add('d-none');
    this.yearsTableNode.classList.add('d-none');
    this.monthsTableNode.classList.remove('d-none');
    this.prevYearNode.classList.remove('d-none');
    this.nextYearNode.classList.remove('d-none');
}

DatePicker.prototype.showYearsDecade = function () {
    this.dateTableNode.classList.add('d-none');
    this.monthsTableNode.classList.add('d-none');
    this.prevYearNode.classList.add('d-none');
    this.nextYearNode.classList.add('d-none');
    this.yearsTableNode.classList.remove('d-none');
    this.prevDecadeNode.classList.remove('d-none');
    this.nextDecadeNode.classList.remove('d-none');
}

DatePicker.prototype.showDaysMonth = function () {
    this.monthsTableNode.classList.add('d-none');
    this.yearsTableNode.classList.add('d-none');
    this.prevDecadeNode.classList.add('d-none');
    this.nextDecadeNode.classList.add('d-none');
    this.dateTableNode.classList.remove('d-none');
    this.prevMonthNode.classList.remove('d-none');
    this.nextMonthNode.classList.remove('d-none');
}

DatePicker.prototype.hideLastRow = function () {
    this.lastRowNode.style.display = 'none';
};

DatePicker.prototype.showLastRow = function () {
    this.lastRowNode.style.display = 'table-row';
};

DatePicker.prototype.setFocusDay = function (flag) {

    if (typeof flag !== 'boolean') {
        flag = true;
    }

    var fd = this.focusDay;

    function checkDay(d) {
        d.domNode.setAttribute('tabindex', '-1');
        if ((d.day.getDate() == fd.getDate()) &&
            (d.day.getMonth() == fd.getMonth()) &&
            (d.day.getFullYear() == fd.getFullYear())) {
            d.domNode.setAttribute('tabindex', '0');
            //Control Meses
            var buttonMonth = this.monthsTableNode.querySelector("button[data-date='" + (d.day.getMonth() + 1) + "']");
            this.buttonsCellsMonths.forEach(function (e) {
                e.setAttribute('tabindex', '-1');
            })
            buttonMonth.setAttribute('tabindex', '0')
            //Control Años
            var buttonYear = this.yearsTableNode.querySelector("button[data-date='" + d.day.getFullYear() + "']");
            this.buttonsCellsYears.forEach(function (e) {
                e.setAttribute('tabindex', '-1');
            })

            buttonYear.setAttribute('tabindex', '0')

            if (flag) {
                d.domNode.focus();
            }
        }
    }

    this.days.forEach(checkDay.bind(this));
};

DatePicker.prototype.updateDay = function (day) {
    var d = this.focusDay;
    this.focusDay = day;
    if ((d.getMonth() !== day.getMonth()) ||
        (d.getFullYear() !== day.getFullYear())) {
        this.updateGrid();
        this.setFocusDay();
    }
};

DatePicker.prototype.getDaysInLastMonth = function () {
    var fd = this.focusDay;
    var lastDayOfMonth = new Date(fd.getFullYear(), fd.getMonth(), 0);
    return lastDayOfMonth.getDate();
};

DatePicker.prototype.getDaysInMonth = function () {
    var fd = this.focusDay;
    var lastDayOfMonth = new Date(fd.getFullYear(), fd.getMonth() + 1, 0);
    return lastDayOfMonth.getDate();
};

DatePicker.prototype.show = function () {

    var desplegable = this.inputNode.closest('.desplegable-selected-option');
    desplegable.classList.add('desplegable-arrow-active');
    this.dialogNode.style.display = 'block';
    this.dialogNode.style.zIndex = 2;

    this.getDateInput();
    this.updateGrid();
    this.setFocusDay();

};

DatePicker.prototype.isOpen = function () {
    return window.getComputedStyle(this.dialogNode).display !== 'none';
};

DatePicker.prototype.hide = function () {
    var desplegable = this.inputNode.closest('.desplegable-selected-option');
    desplegable.classList.remove('desplegable-arrow-active');
    this.dialogNode.style.display = 'none';

    this.hasFocusFlag = false;
    this.dateInput.setFocus();
};

DatePicker.prototype.handleBackgroundMouseDown = function (event) {
    if (!this.inputNode.contains(event.target) &&
        !this.dialogNode.contains(event.target)) {

        this.isMouseDownOnBackground = true;

        if (this.isOpen()) {
            this.hide();
            event.stopPropagation();
            event.preventDefault();
        }
    }
};

DatePicker.prototype.handleBackgroundMouseUp = function () {
    this.isMouseDownOnBackground = false;
};

DatePicker.prototype.showMonthsYearNode = function (event) {
    var flag = false;

    switch (event.type) {
        case 'keydown':

            switch (event.keyCode) {
                case this.keyCode.ENTER:
                case this.keyCode.SPACE:

                    this.setTextboxDate();

                    this.hide();
                    flag = true;
                    break;

                case this.keyCode.TAB:
                    if (!event.shiftKey) {
                        this.prevYearNode.focus();
                        flag = true;
                    }
                    break;

                case this.keyCode.ESC:
                    this.hide();
                    flag = true;
                    break;

                default:
                    break;

            }
            break;

        case 'click':
            this.setTextboxDate();
            this.hide();
            flag = true;
            break;

        default:
            break;
    }

    if (flag) {
        event.stopPropagation();
        event.preventDefault();
    }
};

DatePicker.prototype.handleNextYearButton = function (event) {
    var flag = false;

    switch (event.type) {

        case 'keydown':

            switch (event.keyCode) {
                case this.keyCode.ESC:
                    this.hide();
                    flag = true;
                    break;

                case this.keyCode.ENTER:
                case this.keyCode.SPACE:
                    this.moveToNextYear();
                    this.setFocusDay(false);
                    flag = true;
                    break;
            }

            break;

        case 'click':
            this.moveToNextYear();
            this.setFocusDay(false);
            break;

        default:
            break;
    }

    if (flag) {
        event.stopPropagation();
        event.preventDefault();
    }
};

DatePicker.prototype.handlePreviousYearButton = function (event) {
    var flag = false;

    switch (event.type) {

        case 'keydown':

            switch (event.keyCode) {

                case this.keyCode.ENTER:
                case this.keyCode.SPACE:
                    this.moveToPreviousYear();
                    this.setFocusDay(false);
                    flag = true;
                    break;

                case this.keyCode.TAB:
                    if (event.shiftKey) {
                        this.okButtonNode.focus();
                        flag = true;
                    }
                    break;

                case this.keyCode.ESC:
                    this.hide();
                    flag = true;
                    break;

                default:
                    break;
            }

            break;

        case 'click':
            this.moveToPreviousYear();
            this.setFocusDay(false);
            break;

        default:
            break;
    }

    if (flag) {
        event.stopPropagation();
        event.preventDefault();
    }
};

DatePicker.prototype.handleNextMonthButton = function (event) {
    var flag = false;

    switch (event.type) {

        case 'keydown':

            switch (event.keyCode) {
                case this.keyCode.ESC:
                    this.hide();
                    flag = true;
                    break;

                case this.keyCode.ENTER:
                case this.keyCode.SPACE:
                    this.moveToNextMonth();
                    this.setFocusDay(false);
                    flag = true;
                    break;
            }

            break;

        case 'click':
            this.moveToNextMonth();
            this.setFocusDay(false);
            break;

        default:
            break;
    }

    if (flag) {
        event.stopPropagation();
        event.preventDefault();
    }
};

DatePicker.prototype.handlePreviousMonthButton = function (event) {
    var flag = false;

    switch (event.type) {

        case 'keydown':

            switch (event.keyCode) {
                case this.keyCode.ESC:
                    this.hide();
                    flag = true;
                    break;

                case this.keyCode.ENTER:
                case this.keyCode.SPACE:
                    this.moveToPreviousMonth();
                    this.setFocusDay(false);
                    flag = true;
                    break;
            }

            break;

        case 'click':
            this.moveToPreviousMonth();
            this.setFocusDay(false);
            flag = true;
            break;

        default:
            break;
    }

    if (flag) {
        event.stopPropagation();
        event.preventDefault();
    }
};


DatePicker.prototype.handleMonthButton = function (event) {
    var buttonMonth = event.target;
    var month = buttonMonth.getAttribute('data-date');
    this.showDaysMonth();
    this.focusDay.setMonth(month - 1);
    this.buttonsCellsMonths.forEach(function (e) {
        e.setAttribute('tabindex', '-1');
    })
    buttonMonth.setAttribute('tabindex', '0');
    this.updateGrid();
};


DatePicker.prototype.handleYearButton = function (event) {
    var buttonYear = event.target;
    var year = buttonYear.getAttribute('data-date');
    this.showMonthsYear();
    this.focusDay.setFullYear(year);
    this.MonthYearNode.innerHTML = year;
    this.updateYearGrid();
};

DatePicker.prototype.moveToNextYear = function () {
    var fd = this.focusDay;
    this.MonthYearNode.innerHTML = fd.getFullYear() + 1;
    this.focusDay.setFullYear(fd.getFullYear() + 1);
};

DatePicker.prototype.moveToPreviousYear = function () {
    var fd = this.focusDay;
    this.MonthYearNode.innerHTML = fd.getFullYear() - 1;
    this.focusDay.setFullYear(fd.getFullYear() - 1);
};

DatePicker.prototype.moveToNextMonth = function () {
    this.focusDay.setMonth(this.focusDay.getMonth() + 1);
    this.updateGrid();
};

DatePicker.prototype.moveToPreviousMonth = function () {
    this.focusDay.setMonth(this.focusDay.getMonth() - 1);
    this.updateGrid();
};

DatePicker.prototype.moveFocusToDay = function (day) {
    var d = this.focusDay;

    this.focusDay = day;

    if ((d.getMonth() != this.focusDay.getMonth()) ||
        (d.getYear() != this.focusDay.getYear())) {
        this.updateGrid();
    }
    this.setFocusDay(false);
};

DatePicker.prototype.moveFocusToNextDay = function () {
    var d = new Date(this.focusDay);
    d.setDate(d.getDate() + 1);
    this.moveFocusToDay(d);
};

DatePicker.prototype.moveFocusToNextWeek = function () {
    var d = new Date(this.focusDay);
    d.setDate(d.getDate() + 7);
    this.moveFocusToDay(d);
};

DatePicker.prototype.moveFocusToPreviousDay = function () {
    var d = new Date(this.focusDay);
    d.setDate(d.getDate() - 1);
    this.moveFocusToDay(d);
};

DatePicker.prototype.moveFocusToPreviousWeek = function () {
    var d = new Date(this.focusDay);
    d.setDate(d.getDate() - 7);
    this.moveFocusToDay(d);
};

DatePicker.prototype.moveFocusToFirstDayOfWeek = function () {
    var d = new Date(this.focusDay);
    d.setDate(d.getDate() - d.getDay());
    this.moveFocusToDay(d);
};

DatePicker.prototype.moveFocusToLastDayOfWeek = function () {
    var d = new Date(this.focusDay);
    d.setDate(d.getDate() + (6 - d.getDay()));
    this.moveFocusToDay(d);
};

DatePicker.prototype.setTextboxDate = function (day) {
    if (day) {
        this.dateInput.setDate(day);
    }
    else {
        this.dateInput.setDate(this.focusDay);
    }
};

DatePicker.prototype.getDateInput = function () {

    var parts = this.dateInput.getDate().split('/');

    if ((parts.length === 3) &&
        Number.isInteger(parseInt(parts[0])) &&
        Number.isInteger(parseInt(parts[1])) &&
        Number.isInteger(parseInt(parts[2]))) {
        this.focusDay = new Date(parseInt(parts[2]), parseInt(parts[0]) - 1, parseInt(parts[1]));
        this.selectedDay = new Date(this.focusDay);
    }
    else {
        // If not a valid date (MM/DD/YY) initialize with todays date
        this.focusDay = new Date();
        this.selectedDay = new Date(0, 0, 1);
    }

};

DatePicker.prototype.getDateForButtonLabel = function (year, month, day) {
    if (typeof year !== 'number' || typeof month !== 'number' || typeof day !== 'number') {
        this.selectedDay = this.focusDay;
    }
    else {
        this.selectedDay = new Date(year, month, day);
    }

    var label = this.dayLabels[this.selectedDay.getDay()];
    label += ' ' + this.monthLabels[this.selectedDay.getMonth()];
    label += ' ' + (this.selectedDay.getDate());
    label += ', ' + this.selectedDay.getFullYear();
    return label;
};

var DatePicker = DatePicker || {};

var CalendarButtonInput = function (inputNode, datepicker) {
    this.inputNode = inputNode;
    this.imageNode = false;

    this.datepicker = datepicker;

    this.defaultLabel = 'Choose Date';

    this.keyCode = Object.freeze({
        'ENTER': 13,
        'SPACE': 32
    });
};

CalendarButtonInput.prototype.init = function () {
    this.inputNode.addEventListener('click', this.handleClick.bind(this));
    this.inputNode.addEventListener('keydown', this.handleKeyDown.bind(this));
    this.inputNode.addEventListener('focus', this.handleFocus.bind(this));
};

CalendarButtonInput.prototype.handleKeyDown = function (event) {
    var flag = false;

    switch (event.keyCode) {

        case this.keyCode.SPACE:
        case this.keyCode.ENTER:
            this.datepicker.show();
            this.datepicker.setFocusDay();
            flag = true;
            break;

        default:
            break;
    }

    if (flag) {
        event.stopPropagation();
        event.preventDefault();
    }
};

CalendarButtonInput.prototype.handleClick = function () {
    if (!this.datepicker.isOpen()) {
        this.datepicker.show();
        this.datepicker.setFocusDay();
    }
    else {
        this.datepicker.hide();
    }

    event.stopPropagation();
    event.preventDefault();

};

CalendarButtonInput.prototype.setLabel = function (str) {
    if (typeof str === 'string' && str.length) {
        str = ', ' + str;
    }
    this.inputNode.setAttribute('aria-label', this.defaultLabel + str);
};

CalendarButtonInput.prototype.setFocus = function () {
    this.inputNode.focus();
};

CalendarButtonInput.prototype.setDate = function (day) {
    this.inputNode.value = (day.getMonth() + 1) + '/' + day.getDate() + '/' + day.getFullYear();
};

CalendarButtonInput.prototype.getDate = function () {
    return this.inputNode.value;
};

CalendarButtonInput.prototype.getDateLabel = function () {
    var label = '';

    var parts = this.inputNode.value.split('/');

    if ((parts.length === 3) &&
        Number.isInteger(parseInt(parts[0])) &&
        Number.isInteger(parseInt(parts[1])) &&
        Number.isInteger(parseInt(parts[2]))) {
        var month = parseInt(parts[0]) - 1;
        var day = parseInt(parts[1]);
        var year = parseInt(parts[2]);

        label = this.datepicker.getDateForButtonLabel(year, month, day);
    }

    return label;
};

CalendarButtonInput.prototype.handleFocus = function () {
    var dateLabel = this.getDateLabel();

    if (dateLabel) {
        this.setLabel('selected date is ' + dateLabel);
    }
    else {
        this.setLabel('');
    }
};